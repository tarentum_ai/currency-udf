## Tarentum Currency UDF - Excel Addin

### Download & Install
You can find installer file link for Windows x86 & x64 below.

[Download Installer](https://bitbucket.org/tarentum_ai/currency-udf/downloads/TarentumCurrencyUDFInstaller.exe)

### Usage
##### Structure
```
EXCH(<currency>, <date>)
```
##### Parameters
- **currency** (required) 
Three letter currency identifier case-insensitive.
- **date** (optional) 
Date to provide data for.
If supplied, should be string in **"YYYY-MM-DD"** format.
If not supplied, latest data is returned.

### Examples

`=EXCH("USD")` returns 1.1076

`=EXCH("USD", "2019-08-01")`  returns 1.1037
