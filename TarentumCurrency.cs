﻿using System;
using System.Net.Http;
using Newtonsoft.Json.Linq;

using ExcelDna.Integration;

namespace CurrencyUDF
{
    public static class TarentumCurrency
    {
        [ExcelFunction(Name = "EXCH", Description = "Returns exchange rate from EUR to a requested currency at a requested date")]
        public static double Exchange(string currency, object dateInput)
        {
            string date = Optional.Check(dateInput, "latest");
            currency = currency.ToUpper();
            string URL = String.Format("https://api.ratesapi.io/api/{0}?base=EUR&symbols={1}",date, currency);

            using (var httpClient = new HttpClient())
            {
                var response = httpClient.GetStringAsync(new Uri(URL)).Result;
                JObject data = JObject.Parse(response);
                return (double)data.SelectToken("rates").SelectToken(currency);
            }
        }
    }
}

internal static class Optional
{
    internal static string Check(object arg, string defaultValue)
    {
        if (arg is string)
        {
            return (string)arg;
        } else if (arg is ExcelMissing)
        {
            return defaultValue;
        } else
        {
            return arg.ToString();
        }
    }
}
